﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace VoterApp
{
    internal class VotingRepo
    {
        private readonly string _apiCandidatesUrl = @"http://localhost:60013/api/candidates";
        private readonly string _apiVotesUrl = @"http://localhost:60013/api/votes";


        public async Task<HttpResponseMessage> Vote(int id, string pesel)
        {
            var newVote = JsonConvert.SerializeObject(new Vote {CandidateId = id, Pesel = pesel});
            using (var client = new HttpClient())
            {
                return await client.PostAsync(_apiVotesUrl, new StringContent(newVote, Encoding.UTF8, "application/json"));
            }
        }

        public async Task<List<Candidate>> ImportCandidatesFromApi()
        {
            var json = string.Empty;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync(_apiCandidatesUrl);
                if (!response.IsSuccessStatusCode) return null;
                json = await response.Content.ReadAsStringAsync();
                using (var jsonFile = new StringReader(json))
                {
                    return (List<Candidate>) new JsonSerializer().Deserialize(jsonFile, typeof(List<Candidate>));
                }
            }
        }
    }
}