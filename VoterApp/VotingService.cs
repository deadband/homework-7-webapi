﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace VoterApp
{
    public class VotingService
    {
        private readonly VotingRepo votingRepo = new VotingRepo();
        private ConsoleKey selection = new ConsoleKeyInfo().Key;

        public void MenuHandler()
        {
            while (selection != ConsoleKey.D3)
            {
                DisplayMenu();
                selection = Console.ReadKey().Key;
                Console.WriteLine("\n");
                switch (selection)
                {
                    case ConsoleKey.D1:
                        DisplayCandidates();                      
                        break;
                    case ConsoleKey.D2:
                        DisplayCandidates();
                        Vote();
                        break;
                    default:
                        break;
                }
            }
        }

        private void DisplayCandidates()
        {
            var candidates = votingRepo.ImportCandidatesFromApi().Result;
            if (candidates == null)
            {
                Console.WriteLine("Candidate list import failed.");
                Console.WriteLine("\nPress any key to continue ...");
                Console.ReadKey();
                return;
            }
            if (!IsAnyCandidate(candidates)) return;
            for (int i = 0; i < candidates.Count; i++)
            {
                Console.WriteLine(
                    $"{i + 1}. {candidates[i].Surname} {candidates[i].FirstName} {candidates[i].SecondName}");
            }
            Console.WriteLine("\nPress any key to continue ...");
            Console.ReadKey();
        }

        private void Vote()
        {
            var candidates = votingRepo.ImportCandidatesFromApi().Result;
            if (candidates == null || candidates.Count == 0) return;
            Console.Write("\nEnter PESEL number: ");
            var pesel = PeselOk(Console.ReadLine());
            Console.Write($"Enter your candidate number [1-{candidates.Count}]: ");
            var number = SelectionOk(Console.ReadLine(), candidates.Count);
            var voteSuccess = votingRepo.Vote(number, pesel).Result.IsSuccessStatusCode;

            if (voteSuccess)
            {
                Console.WriteLine("Vote Succesfull.");
            }
            else Console.WriteLine("Vote not valid or you already voted.");

            Console.WriteLine("\nPress any key to continue ...");
            Console.ReadKey();
        }

        private void DisplayMenu()
        {
            Console.Clear();
            Console.WriteLine("*** VOTER MENU ***");
            Console.WriteLine("1. Display candidates");
            Console.WriteLine("2. Display candidates & vote");
            Console.WriteLine("3. Exit program.");
            Console.Write("Selecton: ");
        }

        private string PeselOk(string pesel)
        {
            var isPeselOk = Regex.IsMatch(pesel, @"^\d{11}$");
            while (!isPeselOk)
            {
                Console.Write("Wrong pesel provide correct 11 digit number: ");
                pesel = Console.ReadLine();
                isPeselOk = Regex.IsMatch(pesel, @"^\d{11}$");
            }

            return pesel;
        }

        private int SelectionOk(string number, int max)
        {
            var parsePossible = int.TryParse(number, out int result);
            while (!parsePossible || result > max || result < 1)
            {
                Console.Write("Incorrect Selection. Type again: ");
                number = Console.ReadLine();
                parsePossible = int.TryParse(number, out result);
            }
            return result;
        }

        private static bool IsAnyCandidate<T>(List<T> list)
        {
            if (list.Count != 0) return true;
            Console.WriteLine("No candidates on the list. Wait for the voting commision to add candidates.");
            Console.WriteLine("\nPress any key to continue ...");
            Console.ReadKey();
            return false;
        }
    }
}