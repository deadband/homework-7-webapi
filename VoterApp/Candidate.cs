﻿namespace VoterApp
{
    internal class Candidate
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public int VoteCount { get; set; }
    }
}