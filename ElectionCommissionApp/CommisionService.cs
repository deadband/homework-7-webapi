﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Timers;

namespace ElectionCommissionApp
{
    public class CommisionService
    {
        private readonly CommisionRepo _commisionRepo = new CommisionRepo();
        private readonly EventHandler eventHandler;
        private readonly Timer t = new Timer(10000);
        private ConsoleKey _selection = new ConsoleKey();

        public CommisionService()
        {
            eventHandler += DisplayResultsAndMenu;
            t.Start();
        }

        public void MenuHandler()
        {           
            while (_selection != ConsoleKey.D3)
            {
                eventHandler?.Invoke(this, EventArgs.Empty);
                t.Elapsed += DisplayResultsAndMenu;               
                _selection = Console.ReadKey().Key;
                Console.WriteLine("\n");
                switch (_selection)
                {
                    case ConsoleKey.D1:
                        AddCandidate();
                        break;
                    case ConsoleKey.D2:
                        DisplayCandidates();
                        break;                 
                    default:
                        t.Elapsed -= DisplayResultsAndMenu;
                        break;
                }
            }
        }

        private void DisplayResultsAndMenu(object sender, EventArgs e)
        {
            Console.Clear();
            var candidates = _commisionRepo.ImportCandidatesFromApi().Result.OrderByDescending(c => c.VoteCount).ToList();
            if (candidates != null && candidates.Count !=0)
            {
                Console.WriteLine("LIVESCORE");
                Console.WriteLine("Votes - Candidate\n");
                for (int i = 0; i < candidates.Count; i++)
                {
                    Console.WriteLine($"{candidates[i].VoteCount} - {candidates[i].Surname} {candidates[i].FirstName} {candidates[i].SecondName}");
                }
                Console.WriteLine();
            }

            Console.WriteLine("** COMMISION MENU **");
            Console.WriteLine("1. Add candidate.");
            Console.WriteLine("2. Display candidates.");
            Console.WriteLine("3. Exit Program.");
            Console.Write("Selecton: ");        
        }

        private void AddCandidate()
        {
            t.Elapsed -= DisplayResultsAndMenu;
            Console.Write("Candidate's surname: ");
            var surname = InputOk(Console.ReadLine());
            Console.Write("Candidate's first name: ");
            var firstName = InputOk(Console.ReadLine());
            Console.Write("Candidate's second name: ");
            var secondName = InputOk(Console.ReadLine());
            var postSuccess = _commisionRepo.AddCandidate(firstName, secondName, surname).Result.IsSuccessStatusCode;
            if (postSuccess)
            {
                Console.WriteLine("\nCandidate Succesfully added.");
            }
            else
            {
                Console.WriteLine("\nError. Candidate not added. Try again.");
            }
            Console.WriteLine("\nPress any key to continue ...");
            Console.ReadKey();
        }

        private void DisplayCandidates()
        {
            t.Elapsed -= DisplayResultsAndMenu;
            var candidates = _commisionRepo.ImportCandidatesFromApi().Result;
            if (candidates == null)
            {
                Console.WriteLine("Candidate list import failed");
                Console.WriteLine("\nPress any key to continue ...");
                Console.ReadKey();
                return;
            }
            if (!IsAnyCandidate(candidates)) return;
            for (int i = 0; i < candidates.Count; i++)
            {
                Console.WriteLine(
                    $"{i + 1}. {candidates[i].Surname} {candidates[i].FirstName} {candidates[i].SecondName}");
            }
            Console.WriteLine("\nPress any key to continue ...");
            Console.ReadKey();
        }

        private string InputOk(string input)
        {
            var isInputOk = Regex.IsMatch(input, @"^[a-zA-Z]{2,25}$");
            while (!isInputOk)
            {
                Console.Write("Wrong input provide correct data: ");
                input = Console.ReadLine();
                isInputOk = Regex.IsMatch(input, @"^[a-zA-Z]{2,25}$");
            }

            return input;
        }

        private bool IsAnyCandidate<T>(List<T> list)
        {
            if (list.Count != 0) return true;
            Console.WriteLine("Add any candidate first!");
            Console.WriteLine("\nPress any key to continue ...");
            Console.ReadKey();
            return false;
        }
    }
}