﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ElectionCommissionApp
{
    internal class CommisionRepo
    {
        private readonly string _url = @"http://localhost:60013/api/candidates";

        public async Task<HttpResponseMessage> AddCandidate(string firstName, string secondName, string surname)
        {
            var newCandidate = JsonConvert.SerializeObject(new Candidate
                {FirstName = firstName, SecondName = secondName, Surname = surname, VoteCount = 0});

            using (var client = new HttpClient())
            {
                return await client.PostAsync(_url, new StringContent(newCandidate, Encoding.UTF8, "application/json"));
            }
        }

        public async Task<List<Candidate>> ImportCandidatesFromApi()
        {
            var json = string.Empty;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.GetAsync(_url);
                if (!response.IsSuccessStatusCode) return null;
                json = await response.Content.ReadAsStringAsync();
                using (var jsonFile = new StringReader(json))
                {
                    return (List<Candidate>) new JsonSerializer().Deserialize(jsonFile, typeof(List<Candidate>));
                }
            }
        }
    }
}