﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FreeElectionData
{
    public class Candidate
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        public int VoteCount { get; set; }
    }
}