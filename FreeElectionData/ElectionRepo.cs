﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using Newtonsoft.Json;

namespace FreeElectionData
{
    public class ElectionRepo
    {
        private readonly string _candidatesFilePath = "candidates.json";
        private readonly string _votersFilePath = "voters.json";

        public HttpResponseMessage AddCandidate(Candidate candidate)
        {
            if (candidate.VoteCount != 0) return new HttpResponseMessage(HttpStatusCode.BadRequest);
            var candidates = LoadCandidates();
            candidate.Id = candidates.Count !=0 ? candidates.Select(x => x.Id).Max() + 1 : 1;
            candidates.Add(candidate);
            candidates = candidates.OrderBy(c => c.Surname).ToList();
            SaveCandidates(candidates);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        public List<Candidate> GetCandidates()
        {
            return LoadCandidates();
        }

        public void RemoveCandidate(int id)
        {
            var candidates = LoadCandidates();
            var candidateToRemove = candidates.FirstOrDefault(c => c.Id == id);
            candidates.Remove(candidateToRemove);
            SaveCandidates(candidates);
        }

        public HttpResponseMessage Vote(Vote vote)
        {
            var voters = LoadVoters();
            var candidates = LoadCandidates();
            var MetConditionsToVote = !voters.Select(v => v.Pesel).Contains(vote.Pesel) &&
                                      candidates.Select(c => c.Id).Contains(vote.CandidateId);

            if (MetConditionsToVote)
            {
                candidates.FirstOrDefault(c => c.Id == vote.CandidateId).VoteCount++;
                voters.Add(vote);
                SaveCandidates(candidates);
                SaveVoters(voters);
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }      

        private List<Vote> LoadVoters()
        {
            var voters = new List<Vote>();
            if (File.Exists(_votersFilePath))
            {
                using (var file = new StreamReader(_votersFilePath))
                {
                    return (List<Vote>) new JsonSerializer().Deserialize(file, typeof(List<Vote>));
                }
            }
            return voters;
        }

        private List<Candidate> LoadCandidates()
        {
            if (File.Exists(_candidatesFilePath))
            {
                using (var file = new StreamReader(_candidatesFilePath))
                {
                    return (List<Candidate>) new JsonSerializer().Deserialize(file, typeof(List<Candidate>));
                }
            }
           return new List<Candidate>();
        }

        private void SaveCandidates(List<Candidate> candidates)
        {
            using (var file = new StreamWriter(_candidatesFilePath))
            {
                new JsonSerializer().Serialize(file, candidates);
            }
        }

        private void SaveVoters(List<Vote> voters)
        {
            using (var file = new StreamWriter(_votersFilePath))
            {
                new JsonSerializer().Serialize(file, voters);
            }
        }       
    }
}