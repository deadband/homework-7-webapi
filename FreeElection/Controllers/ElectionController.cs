﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using FreeElectionData;

namespace FreeElection.Controllers
{
    public class ElectionController : ApiController
    {
        private ElectionRepo _electionRepo;

        public ElectionController()
        {
            _electionRepo = new ElectionRepo();
        }

        [HttpGet]   
        [Route("api/candidates")]
        [Route("api/votes/results")]
        public List<Candidate> Get()
        {
            return _electionRepo.GetCandidates();
        }

        [HttpPost]
        [Route("api/candidates")]
        public HttpResponseMessage Post([FromBody]Candidate candidate)
        {
            return  _electionRepo.AddCandidate(candidate);
        }

        [HttpPost]
        [Route("api/votes")]
        // POST: api/Votes
        public HttpResponseMessage Post(Vote vote)
        {
            return _electionRepo.Vote(vote);
        }

        //[HttpGet]   
        //[Route("api/candidates/{id:int}")] 
        //public Candidate Get(int id)
        //{
        //    return _electionRepo.GetById(id);
        //}

        //[HttpDelete]
        //[Route("api/candidates/{id:int}")]
        //public void Delete(int id)
        //{
        //    _electionRepo.RemoveCandidate(id);
        //}
    }
}